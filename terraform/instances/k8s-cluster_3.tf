resource "aws_key_pair" "key_3" {
  key_name   = "${local.init_tags["name"]}-input"
  public_key = file(var.public_key_path)

  provider = aws.region_3
}

resource "aws_instance" "kube_awx_3" {
  ami                    = data.aws_ami.debian_ami_3.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.key.key_name
  vpc_security_group_ids = [aws_security_group.k8s_cluster_3.id]
  subnet_id              = data.terraform_remote_state.network.outputs.public_subnets_3[0]

  associate_public_ip_address = true

  root_block_device {
    delete_on_termination = true
    volume_type           = "gp2"
    volume_size           = "10"
  }

  tags = {
    Name        = "${local.init_tags["name"]}-kube-awx}"
    Environment = local.init_tags["env"]
    Owner       = local.init_tags["owner"]
    Stack       = local.init_tags["stack"]
  }

  provider = aws.region_3
}


resource "aws_security_group" "k8s_cluster_3" {
  vpc_id = data.terraform_remote_state.network.outputs.vpc_id_3

  name        = "${local.init_tags["name"]}-k8s-cluster"
  description = "Applied to k8s-cluster"

  tags = {
    Environment = local.init_tags["env"]
    Owner       = local.init_tags["owner"]
    Stack       = local.init_tags["stack"]
  }
  provider = aws.region_3
}

resource "aws_security_group_rule" "k8s_cluster_all_in_3" {
  security_group_id = aws_security_group.k8s_cluster_3.id

  type      = "ingress"
  protocol  = "all"
  from_port = 0
  to_port   = 0

  cidr_blocks = ["0.0.0.0/0"]
  provider    = aws.region_3
}

resource "aws_security_group_rule" "k8s_cluster_all_out_3" {
  security_group_id = aws_security_group.k8s_cluster_3.id

  type      = "egress"
  protocol  = "all"
  from_port = 0
  to_port   = 0

  cidr_blocks = ["0.0.0.0/0"]
  provider    = aws.region_3
}

