output "ansible_inventory" {
  value = <<EOF
awx_1 ansible_host=${aws_instance.awx_1.public_ip} tinc_vpn_ip=10.91.91.10
awx_2 ansible_host=${aws_instance.kube_awx_2.public_ip} tinc_vpn_ip=10.91.91.20
awx_3 ansible_host=${aws_instance.kube_awx_3.public_ip} tinc_vpn_ip=10.91.91.30

[awx]
awx_1
awx_2
awx_3

[postgresql_ha:children]
postgresql_ha_primary
postgresql_ha_standby

[postgresql_ha_primary]
awx_1 repmgr_node_id=1

[postgresql_ha_standby]
awx_2 repmgr_node_id=2
awx_3 repmgr_node_id=3
EOF

}

output "ssh_config" {
  value = <<EOF
Host *
  IdentityFile proto-aws.master-key
  StrictHostKeyChecking False
  User admin

EOF

}
