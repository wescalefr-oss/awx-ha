data "terraform_remote_state" "network" {
  backend = "local"

  config = {
    path = "../network/terraform.tfstate"
  }
}

variable "masters_nb" {
  default = 3
}

variable "nodes_nb" {
  default = 0
}

variable "private_key_path" {
  type    = string
  default = "../../proto-aws.master-key"
}

variable "aws_region_1" {
  type    = string
  default = "eu-west-1"
}

variable "aws_region_2" {
  type    = string
  default = "eu-west-3"
}

variable "aws_region_3" {
  type    = string
  default = "eu-central-1"
}

variable "public_key_path" {
  type    = string
  default = "../../proto-aws.master-key.pub"
}

variable "instance_type" {
  default = "t3.large"
}

variable "input_instance_type" {
  default = "t3.large"
}

variable "output_instance_type" {
  default = "t3.large"
}

data "aws_ami" "debian_ami_1" {
  most_recent = true

  filter {
    name   = "name"
    values = ["debian-stretch-hvm-x86_64-gp2-*"]
  }

  owners = ["379101102735"]

  provider = aws.region_1
}

data "aws_ami" "debian_ami_2" {
  most_recent = true

  filter {
    name   = "name"
    values = ["debian-stretch-hvm-x86_64-gp2-*"]
  }

  owners = ["379101102735"]

  provider = aws.region_2
}

data "aws_ami" "debian_ami_3" {
  most_recent = true

  filter {
    name   = "name"
    values = ["debian-stretch-hvm-x86_64-gp2-*"]
  }

  owners = ["379101102735"]

  provider = aws.region_3
}

locals {
  az_list = data.terraform_remote_state.network.outputs.az_list
  init_tags = {
    basename = data.terraform_remote_state.network.outputs.init_tags.name
    name     = "${data.terraform_remote_state.network.outputs.init_tags.name}-${data.terraform_remote_state.network.outputs.init_tags.env}"
    env      = data.terraform_remote_state.network.outputs.init_tags.env
    owner    = data.terraform_remote_state.network.outputs.init_tags.owner
    stack    = data.terraform_remote_state.network.outputs.init_tags.stack
  }
}

provider "aws" {
  region = var.aws_region_1
}

provider "aws" {
  alias  = "region_1"
  region = var.aws_region_1
}

provider "aws" {
  alias  = "region_2"
  region = var.aws_region_2
}

provider "aws" {
  alias  = "region_3"
  region = var.aws_region_3
}

