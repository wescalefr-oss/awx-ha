resource "aws_key_pair" "key" {
  key_name   = "${local.init_tags["name"]}-input"
  public_key = file(var.public_key_path)

  provider = aws.region_1
}

resource "aws_instance" "awx_1" {
  ami                    = data.aws_ami.debian_ami_1.image_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.key.key_name
  vpc_security_group_ids = [aws_security_group.awx_cluster.id]
  subnet_id              = data.terraform_remote_state.network.outputs.public_subnets_1[0]

  associate_public_ip_address = true

  root_block_device {
    delete_on_termination = true
    volume_type           = "gp2"
    volume_size           = "10"
  }

  tags = {
    Name        = "${local.init_tags["name"]}-awx}"
    Environment = local.init_tags["env"]
    Owner       = local.init_tags["owner"]
    Stack       = local.init_tags["stack"]
  }

  provider = aws.region_1
}


resource "aws_security_group" "awx_cluster" {
  vpc_id = data.terraform_remote_state.network.outputs.vpc_id_1

  name        = "${local.init_tags["name"]}-awx-cluster"
  description = "Applied to awx-cluster"

  tags = {
    Environment = local.init_tags["env"]
    Owner       = local.init_tags["owner"]
    Stack       = local.init_tags["stack"]
  }
  provider = aws.region_1
}

resource "aws_security_group_rule" "awx_cluster_all_in" {
  security_group_id = aws_security_group.awx_cluster.id

  type      = "ingress"
  protocol  = "all"
  from_port = 0
  to_port   = 0

  cidr_blocks = ["0.0.0.0/0"]
  provider    = aws.region_1
}

resource "aws_security_group_rule" "awx_cluster_all_out" {
  security_group_id = aws_security_group.awx_cluster.id

  type      = "egress"
  protocol  = "all"
  from_port = 0
  to_port   = 0

  cidr_blocks = ["0.0.0.0/0"]
  provider    = aws.region_1
}
