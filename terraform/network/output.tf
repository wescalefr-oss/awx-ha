output "public_subnets_1" {
  value = [
    module.vpc_layout_1.public_subnet_id_a,
    module.vpc_layout_1.public_subnet_id_b,
    module.vpc_layout_1.public_subnet_id_c
  ]
}

output "vpc_id_1" {
  value = module.vpc_layout_1.vpc_id
}


output "public_subnets_2" {
  value = [
    module.vpc_layout_2.public_subnet_id_a,
    module.vpc_layout_2.public_subnet_id_b,
    module.vpc_layout_2.public_subnet_id_c
  ]
}

output "vpc_id_2" {
  value = module.vpc_layout_2.vpc_id
}



output "public_subnets_3" {
  value = [
    module.vpc_layout_3.public_subnet_id_a,
    module.vpc_layout_3.public_subnet_id_b,
    module.vpc_layout_3.public_subnet_id_c
  ]
}

output "vpc_id_3" {
  value = module.vpc_layout_3.vpc_id
}


output "init_tags" {
    value = {
    basename  = "proto-aws"
    name  = "proto-aws"
    env   = "demo"
    owner = "proto-aws"
    stack = "kubi"
  }
}

output "az_list" {
  value = ["a","b","c"]
}