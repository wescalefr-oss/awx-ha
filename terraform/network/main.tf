provider "aws" {
  region = var.aws_region_1
}

provider "aws" {
  alias  = "region_1"
  region = var.aws_region_1
}

provider "aws" {
  alias  = "region_2"
  region = var.aws_region_2
}

provider "aws" {
  alias  = "region_3"
  region = var.aws_region_3
}

module "vpc_layout_1" {
  source = "./mod_multi_az_vpc"

  vpc_cidr = var.vpc_cidr_1

  region = var.aws_region_1

  environment = "proto-aws"
  stack       = "network-landscape"
  cost        = "global"

  az_list = ["a","b","c"]
  providers = {
    aws =aws.region_1
  }
}

module "nat_layout_1" {
  source           = "./mod_nat"
  deploy_env       = "demo"
  subnet_id_a      = module.vpc_layout_1.public_subnet_id_a
  subnet_id_b      = module.vpc_layout_1.public_subnet_id_b
  subnet_id_c      = module.vpc_layout_1.public_subnet_id_c
  route_table_id_a = module.vpc_layout_1.private_subnet_route_table_id_a
  route_table_id_b = module.vpc_layout_1.private_subnet_route_table_id_b
  route_table_id_c = module.vpc_layout_1.private_subnet_route_table_id_c
  deploy_region    = var.aws_region_1

  providers = {
    aws =aws.region_1
  }
}



module "vpc_layout_2" {
  source = "./mod_multi_az_vpc"

  vpc_cidr = var.vpc_cidr_2

  region = var.aws_region_2

  environment = "proto-aws"
  stack       = "network-landscape"
  cost        = "global"

  az_list = ["a","b","c"]
  providers = {
    aws =aws.region_2
  }
}

module "nat_layout_2" {
  source           = "./mod_nat"
  deploy_env       = "demo"
  subnet_id_a      = module.vpc_layout_2.public_subnet_id_a
  subnet_id_b      = module.vpc_layout_2.public_subnet_id_b
  subnet_id_c      = module.vpc_layout_2.public_subnet_id_c
  route_table_id_a = module.vpc_layout_2.private_subnet_route_table_id_a
  route_table_id_b = module.vpc_layout_2.private_subnet_route_table_id_b
  route_table_id_c = module.vpc_layout_2.private_subnet_route_table_id_c
  deploy_region    = var.aws_region_2

  providers = {
    aws =aws.region_2
  }
}



module "vpc_layout_3" {
  source = "./mod_multi_az_vpc"

  vpc_cidr = var.vpc_cidr_3

  region = var.aws_region_3

  environment = "proto-aws"
  stack       = "network-landscape"
  cost        = "global"

  az_list = ["a","b","c"]
  providers = {
    aws =aws.region_3
  }
}

module "nat_layout_3" {
  source           = "./mod_nat"
  deploy_env       = "demo"
  subnet_id_a      = module.vpc_layout_3.public_subnet_id_a
  subnet_id_b      = module.vpc_layout_3.public_subnet_id_b
  subnet_id_c      = module.vpc_layout_3.public_subnet_id_c
  route_table_id_a = module.vpc_layout_3.private_subnet_route_table_id_a
  route_table_id_b = module.vpc_layout_3.private_subnet_route_table_id_b
  route_table_id_c = module.vpc_layout_3.private_subnet_route_table_id_c
  deploy_region    = var.aws_region_3

  providers = {
    aws = aws.region_3
  }
}


