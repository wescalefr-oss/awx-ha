# ---------------------------------------

variable "aws_region_1" {
  type    = string
  default = "eu-west-1"
}

variable "vpc_cidr_1" {
  type    = string
  default = "10.10.0.0/16"
}

# ---------------------------------------

variable "aws_region_2" {
  type    = string
  default = "eu-west-3"
}

variable "vpc_cidr_2" {
  type    = string
  default = "10.20.0.0/16"
}

# ---------------------------------------

variable "aws_region_3" {
  type    = string
  default = "eu-central-1"
}

variable "vpc_cidr_3" {
  type    = string
  default = "10.30.0.0/16"
}

# ---------------------------------------
