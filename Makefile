mitogen:
	ansible-playbook -c local mitogen.yml
clean:
	rm -rf dist/
	rm *.retry


install-tinc:
	ansible-playbook ansible/install-tinc.yml

install-pg:
	ansible-playbook ansible/install-pg.yml

install-awx:
	ansible-playbook ansible/install-awx.yml -b

ssh-1:
	bash -c "ssh -F ssh.cfg `cat hosts | grep ansible_host | grep awx_1  | cut -d '=' -f2 | cut -d ' ' -f1`"

ssh-2:
	bash -c "ssh -F ssh.cfg `cat hosts | grep ansible_host | grep awx_2  | cut -d '=' -f2 | cut -d ' ' -f1`"

ssh-3:
	bash -c "ssh -F ssh.cfg `cat hosts | grep ansible_host | grep awx_3  | cut -d '=' -f2 | cut -d ' ' -f1`"

install-requirements: mitogen
	ansible-galaxy install -fr requirements.yml

ping:
	ansible -m ping awx

tf-apply-network:
	bash -c "cd terraform/network && terraform init && terraform apply -auto-approve"

tf-destroy-network:
	bash -c "cd terraform/network && terraform init && terraform destroy -force"

tf-apply-instances:
	bash -c "cd terraform/instances && terraform init && terraform apply -auto-approve"

tf-destroy-instances:
	bash -c "cd terraform/instances && terraform init && terraform destroy -force"

failure-1:
	bash -c "cd terraform/instances && terraform taint aws_instance.awx_1 && terraform apply -auto-approve"

failure-2:
	bash -c "cd terraform/instances && terraform taint aws_instance.awx_2 && terraform apply -auto-approve"

tf-output:
	bash -c "cd terraform/instances && terraform output ssh_config > ../../ssh.cfg"
	bash -c "cd terraform/instances && terraform output ansible_inventory > ../../hosts"

tf-reset: tf-destroy-instances tf-destroy-network tf-apply-network tf-apply-instances tf-output
